require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  spec.rspec_opts = '--exclude-pattern ./spec/cucumber/wire/connection\*_spec.rb'
end
